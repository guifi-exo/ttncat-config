# Configuració de dispositius de tunneling TTNCat

Aquest repositori proporciona els arxius de configuració per OpenWRT dels dispositius actualment en servei del sistema de túnels de gestió TTNCat:

* AirGateway d'Ubiquiti Networks
* WT3020 de Nexx
* Edge-X d'Ubiquiti Networks

En els dos casos es recomanen les versions de maquinari que tinguin 8M de flash com a mínim.

## Procediment de compilació

És possible generar una imatge preconfigurada a partir dels arxius inclosos en aquest repositori. Caldrà en primer lloc modificar els arxius:

```
files/etc/config/network
files/etc/config/system
files/etc/config/olsrd
```
concretament caldrà modificar aquests paràmetres:

* Número de port UDP en el servidor de túnels (`files/etc/config/network`)
* Clau privada generada per aquest dispositiu (`files/etc/config/network`)
* Nom de host normalitzat (`files/etc/config/system`)
* IP de túnel wireguard normalitzada (`files/etc/config/network`)
* Prefix de xarxa privada anunciada per OLSR (`files/etc/config/olsrd`)
* IP de Guifi.net assignada (`files/etc/config/network`)

Tot seguit generem un clone de codi font de OpenWRT dins la carpeta del maquinari en concret:

```
ttncat-config/wt3020_ttn# git clone https://git.lede-project.org/source.git lede
```

Des del directori `lede` creat per l'anterior comanda, triem la darrera versió estable a través del tag. Podem llistar els tags disponibles així:
```
ttncat-config/wt3020_ttn/lede# git tag --list
reboot
v17.01.0
v17.01.0-rc1
v17.01.0-rc2
v17.01.1
v17.01.2
v17.01.3
v17.01.4
v17.01.5
v17.01.6
v18.06.0
v18.06.0-rc1
v18.06.0-rc2
v18.06.1
v18.06.2
v18.06.3
v18.06.4
v18.06.5
v19.07.0-rc1
v19.07.0-rc2

ttncat-config/wt3020_ttn/lede# git checkout v18.06.5
```
Ara creem les referències explícites de tots els paquets:
```
ttncat-config/wt3020_ttn/lede# ./scripts/feeds update -a
ttncat-config/wt3020_ttn/lede# ./scripts/feeds install -a
```
Creem els enllaços simbòlics amb els directoris i arxius de configuració prèviament adaptats:

```
ttncat-config/wt3020_ttn/lede# ln -s ../files files
ttncat-config/wt3020_ttn/lede# ln -s ../.config .config
```
Executem el procés de compilació:
```
make
```
si tot va bé, trobarem les imatges a `<build_dir>/bin/targets/<arch_name>/generic/`

## Procediment d'instal·lació de Edge-X
Fins el maig de 2020 no ha aparegut un procediment satisfactori per instal·lar OpenWRT 19.07 en aquest maquinari des del firmware OEM. Les imatges disponibles
del dipòsit oficial d'OpenWRT inclou una imatge de kernel massa gran. Cal doncs usar una imatge a mida per la primera instal·lació des del firmware del fabricant.
Es disposa d'un dipòsit a Github que n'esplica els detalls i proporciona una imatge adequada [aquí](https://github.com/stman/OpenWRT-19.07.2-factory-tar-file-for-Ubiquiti-EdgeRouter-x)

Baixem la imatge `openwrt-ramips-mt7621-ubnt-erx-initramfs-factory.tar` del dipòsit esmentat. Executem les següents comandes:
```
>scp openwrt-ramips-mt7621-ubnt-erx-initramfs-factory.tar ubnt@192.168.1.1:/tmp
>ssh ubnt@192.168.1.1
ubnt@ubnt:/$cd tmp
ubnt@ubnt:/tmp$ add system image openwrt-ramips-mt7621-ubnt-erx-initramfs-factory.tar
Checking upgrade image...Done
Preparing to upgrade...Done
Copying upgrade image.../usr/bin/ubnt-upgrade: line 509: [: too many arguments
Done
Removing old image...Done
Checking upgrade image...Done
Copying config data...Done
Finishing upgrade...Done
Upgrade completed
```

Un cop enllestit el procés d'instal·lació podem actualitzar amb la imatge definitiva d'OpenWRT amb els mètodes habituals (sysupgrade).
